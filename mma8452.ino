// Distributed with a free-will license.
// Use it any way you want, profit or free, provided it fits in the licenses of its associated works.
// MMA8452Q
// This code is designed to work with the MMA8452Q_I2CS I2C Mini Module available from ControlEverything.com.
// https://www.controleverything.com/content/Accelorometer?sku=MMA8452Q_I2CS#tabs-0-product_tabset-2
 
#include <Wire.h>
#include <ESP8266WiFi.h>
#include <SPI.h>
#include <SD.h>
#include <NTPClient.h>
#include <WiFiUdp.h>
// MMA8452Q I2C address is 0x1C(28)
#define Addr 0x1C
const int chipSelect = D8;
File myFile;

WiFiClient client;
String apiKey = "xxxxxxxxxxxxxxx";     //  Enter your Write API key from ThingSpeak
const char* ssid =  "wifi";     // Enter your WiFi Network's SSID
const char* pass =  "password"; // Enter your WiFi Network's Password
const char* server = "api.thingspeak.com";
float temp;

 
void setup()
{
  // Initialise I2C communication as MASTER
  Wire.begin();
  // Initialise SD Card
  if (!SD.begin(chipSelect)){
     return;
    }
  // Initialise Serial Communication, set baud rate = 9600
  Serial.begin(9600);

  
  Serial.print("Connecting to: ");
       Serial.println(ssid);

         WiFi.begin(ssid, pass);
 
      while (WiFi.status() != WL_CONNECTED) 
     {
            delay(100);
            Serial.print("*");
     }
      Serial.println("");
      Serial.println("***WiFi connected***");


 
  // Start I2C Transmission
  Wire.beginTransmission(Addr);
  // Select control register
  Wire.write(0x2A);
  // StandBy mode
  Wire.write((byte)0x00);
  // Stop I2C Transmission
  Wire.endTransmission();
 
  // Start I2C Transmission
  Wire.beginTransmission(Addr);
  // Select control register
  Wire.write(0x2A);
  // Active mode
  Wire.write(0x01);
  // Stop I2C Transmission
  Wire.endTransmission();
 
  // Start I2C Transmission
  Wire.beginTransmission(Addr);
  // Select control register
  Wire.write(0x0E);
  // Set range to +/- 2g
  Wire.write((byte)0x00);
  // Stop I2C Transmission
  Wire.endTransmission();
  delay(300);
}

 
void loop()
{
  myFile = SD.open("accl.csv", FILE_WRITE);
  unsigned int data[7];
 
  // Request 7 bytes of data
  Wire.requestFrom(Addr, 7);
 
  // Read 7 bytes of data
  // staus, xAccl lsb, xAccl msb, yAccl lsb, yAccl msb, zAccl lsb, zAccl msb
  if(Wire.available() == 7) 
  {
    data[0] = Wire.read();
    data[1] = Wire.read();
    data[2] = Wire.read();
    data[3] = Wire.read();
    data[4] = Wire.read();
    data[5] = Wire.read();
    data[6] = Wire.read();
  }
 
  // Convert the data to 12-bits
  int xAccl = ((data[1] * 256) + data[2]) / 16;
  if (xAccl > 2047)
  {
    xAccl -= 4096;
  }
 
  int yAccl = ((data[3] * 256) + data[4]) / 16;
  if (yAccl > 2047)
  {
    yAccl -= 4096;
  }
 
  int zAccl = ((data[5] * 256) + data[6]) / 16;
  if (zAccl > 2047)
  {
    zAccl -= 4096;
  }

 if (client.connect(server,80))   //   "184.106.153.149" or api.thingspeak.com
      {  
       String sendData = apiKey+"&field1="+String(xAccl)+"\r\n\r\n"+"&field2="+String(yAccl)+"\r\n\r\n"+"&field3="+String(zAccl)+"\r\n\r\n"; 

       Serial.print("Acceleration in X-Axis : ");
       Serial.println(xAccl);
       Serial.print("Acceleration in Y-Axis : ");
       Serial.println(yAccl);
       Serial.print("Acceleration in Z-Axis : ");
       Serial.println(zAccl);
       delay(500);
              
       client.print("POST /update HTTP/1.1\n");
       client.print("Host: api.thingspeak.com\n");
       client.print("Connection: close\n");
       client.print("X-THINGSPEAKAPIKEY: "+apiKey+"\n");
       client.print("Content-Type: application/x-www-form-urlencoded\n");
       client.print("Content-Length: ");
       client.print(sendData.length());
       client.print("\n\n");
       client.print(sendData);
       
    
       Serial.print("Z Axis");
       Serial.print(zAccl);
       Serial.println("Z Axis. Connecting to Thingspeak..");
      }
      
      client.stop();

      Serial.println("Sending....");

        if (myFile) 
    {
    Serial.print("opened accl.csv...");
    myFile.print(xAccl);
    myFile.print(",");
    myFile.print(yAccl);
    myFile.print(",");
    myFile.println(zAccl);
    // close the file:
    myFile.close();
    Serial.println("closed accl.csv.");
    } 
    else 
    {
    // if the file didn't open, print an error:
    Serial.println("error opening accl.csv");
    }
  
 delay(10000);
}
